.. index::
   single: Function reference; Structures
   single: Class reference; Structures

Structures
==========

.. index::
   single: Function reference; Structure container
   single: Class reference; Structure container

StructureContainer
------------------

.. autoclass:: icet.StructureContainer
   :members:
   :undoc-members:
   :inherited-members:

.. index::
   single: Function reference; Structure
   single: Class reference; Structure

Structure
---------

.. autoclass:: icet.Structure
   :members:
   :undoc-members:
   :inherited-members:
