.. _tutorial_advanced_topics:
.. index:: Tutorial, advanced topics

Advanced topics
***************

This section demonstrates the use of :program:`icet` via a set of examples that
showcase different features and modes of usage.

The scripts in this section can be downloaded as
`a single zip archive <../advanced_tutorial.zip>`_.

.. toctree::
   :maxdepth: 1
   :caption: Contents

   structure_enumeration
   cluster_counts
   cluster_space_info
   cluster_vectors
   mapping_structures
   cluster_vector_correlations
   neighbor_list
   permutation_matrix
