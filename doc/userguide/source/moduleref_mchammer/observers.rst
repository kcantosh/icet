.. index::
   single: Monte Carlo; Observers

.. module:: mchammer.observers

Observers
=========

.. index::
   single: Class reference; ClusterExpansionObserver
   single: Monte Carlo; Cluster expansion observer

ClusterExpansionObserver
------------------------

.. autoclass:: ClusterExpansionObserver
   :members:
   :undoc-members:
   :inherited-members:
