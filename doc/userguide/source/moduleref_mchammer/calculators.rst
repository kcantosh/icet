.. index::
   single: Monte Carlo; Calculators

.. module:: mchammer.calculators

Calculators
===========


.. index::
   single: Function reference; ClusterExpansionCalculator
   single: Class reference; ClusterExpansionCalculator
   single: Monte Carlo; Cluster expansion calculator

ClusterExpansionCalculator
--------------------------

.. autoclass:: ClusterExpansionCalculator
   :members:
   :undoc-members:
   :inherited-members:
