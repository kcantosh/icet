.. _tutorial_basics:
.. index:: Tutorial, basics

Basic tutorial
**************

This tutorial serves as a hands-on introduction to :program:`icet` and provides
an overview of its key features.

The scripts and database that are required for this tutorial can be downloaded
as `a single zip archive <../basic_tutorial.zip>`_.

Building a cluster expansion
============================

The first part of this tutorial concerns constructing, validating, and
analyzing a cluster expansion.

.. toctree::
   :maxdepth: 1

   construct_cluster_expansion
   compare_to_target_data
   enumerate_structures
   analyze_ecis

Sampling a cluster expansion
============================

The second part of this tutorial addresses sampling the cluster expansion
constructed above using Monte Carlo simulations and the analysis of these
results.

.. toctree::
   :maxdepth: 1

   run_monte_carlo
   analyze_monte_carlo
